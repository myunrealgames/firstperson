// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "FPSStateBase.generated.h"

/**
 * 
 */
UCLASS()
class FIRSTPERSON_API AFPSStateBase : public AGameStateBase
{
	GENERATED_BODY()
public:
	UFUNCTION(NetMulticast,Reliable)
	void MultiCastOnMissionComplete(APawn* InstigatorPawn, bool bMissionSucceed);
};
