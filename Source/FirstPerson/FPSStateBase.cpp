// Fill out your copyright notice in the Description page of Project Settings.

#include "FPSStateBase.h"
#include "Classes/Engine/World.h"
#include "FPSPlayerController.h"

void AFPSStateBase::MultiCastOnMissionComplete_Implementation(APawn* InstigatorPawn, bool bMissionSucceed)
{
	for (FConstPlayerControllerIterator it = GetWorld()->GetPlayerControllerIterator(); it; it++)
	{
		AFPSPlayerController* playerController = Cast< AFPSPlayerController>(it->Get());
		if (playerController && playerController->IsLocalController())
		{
			playerController->OnMissionSuccess(InstigatorPawn, bMissionSucceed);
			APawn* pawn = playerController->GetPawn();
			if (pawn)
			{
				pawn->DisableInput(nullptr);
			}
		}
	}
}
