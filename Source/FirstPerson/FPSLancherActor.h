// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FPSLancherActor.generated.h"
class UBoxComponent;
class UParticleSystem;
UCLASS()
class FIRSTPERSON_API AFPSLancherActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFPSLancherActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UPROPERTY(VisibleAnywhere, Category = "Components")
	UStaticMeshComponent* mesh;
	UPROPERTY(VisibleAnywhere, Category = "Components")
	UBoxComponent* overlapBox;
	UPROPERTY(VisibleAnywhere, Category = "Components")
	UStaticMeshComponent* plane;
	UPROPERTY(EditInstanceOnly, Category = "Launch")
	float launchSpeed;
	UPROPERTY(EditInstanceOnly, Category = "Launch")
	float launchPitch;
	UPROPERTY(EditDefaultsOnly, Category = "Effects")
	UParticleSystem* particleEffect;

	UFUNCTION()
	void Lanch(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
