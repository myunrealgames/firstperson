// Fill out your copyright notice in the Description page of Project Settings.

#include "BlackHoleActor.h"
#include "Components/SphereComponent.h"
#include <Kismet/GameplayStatics.h>
#include "FirstPersonCharacter.h"

// Sets default values
ABlackHoleActor::ABlackHoleActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));
	RootComponent = MeshComp;
	OuterSpher = CreateDefaultSubobject<USphereComponent>(TEXT("OuterSpher"));
	OuterSpher->SetupAttachment(MeshComp);
	InnerSpher = CreateDefaultSubobject<USphereComponent>(TEXT("InnerSpher"));
	InnerSpher->SetupAttachment(MeshComp);
	InnerSpher->SetSphereRadius(100);
	OuterSpher->SetSphereRadius(300000);
	MeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	InnerSpher->OnComponentBeginOverlap.AddDynamic(this, &ABlackHoleActor::BlackHoleSucktion);


}

// Called when the game starts or when spawned
void ABlackHoleActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABlackHoleActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	TArray<UPrimitiveComponent*> overlappingComps;
	OuterSpher->GetOverlappingComponents(overlappingComps);
	for (auto& overlapMesh : overlappingComps)
	{
		if (overlapMesh && overlapMesh->IsSimulatingPhysics())
		{
			overlapMesh->AddRadialForce(GetActorLocation(), OuterSpher->GetScaledSphereRadius(), -2000, ERadialImpulseFalloff::RIF_Constant, true);
		}
	}

}

void ABlackHoleActor::BlackHoleSucktion(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	PlayEffect();
	if (OtherActor)
	{
		OtherActor->Destroy();
	}

}

void ABlackHoleActor::PlayEffect()
{
	UGameplayStatics::SpawnEmitterAtLocation(this, PickupFX, GetActorLocation());
}