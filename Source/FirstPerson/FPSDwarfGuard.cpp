// Fill out your copyright notice in the Description page of Project Settings.

#include "FPSDwarfGuard.h"
#include "Perception/PawnSensingComponent.h"
#include "DrawDebugHelpers.h"
#include "Engine/Public/TimerManager.h"
#include "FirstPersonGameMode.h"
#include "Classes/Engine/World.h"
#include "Classes/Engine/TargetPoint.h"
#include "Runtime/NavigationSystem/Public/NavigationSystem.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "Net/UnrealNetwork.h"
// Sets default values
AFPSDwarfGuard::AFPSDwarfGuard()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SensingComp = CreateDefaultSubobject<UPawnSensingComponent>(TEXT("SensingComp"));
	guardMode = EGuardMode::Idle;
	pointIndex = 0;
}

// Called when the game starts or when spawned
void AFPSDwarfGuard::BeginPlay()
{
	Super::BeginPlay();
	SensingComp->OnHearNoise.AddDynamic(this, &AFPSDwarfGuard::DwarfHeardNoise);
	originalRotation = GetActorRotation();
	lookAtRotation = originalRotation;
	MoveToNextPoint();
}

void AFPSDwarfGuard::OnDwarfSeePawn(APawn* Pawn)
{
	if (Pawn == nullptr) return;
	DrawDebugSphere(GetWorld(), Pawn->GetActorLocation(), 32.0, 12, FColor::Red,false, 10.0);

	AFirstPersonGameMode* FpsGameMode = Cast<AFirstPersonGameMode>(GetWorld()->GetAuthGameMode());
	if (FpsGameMode)
	{
		FpsGameMode->MissionCompleted(Pawn, false);
	}
	SetGuartMode(EGuardMode::Alert);
}

void AFPSDwarfGuard::DwarfHeardNoise(APawn* NoiseInstigator, const FVector& Location, float Volume)
{
	if (guardMode == EGuardMode::Alert) return;
	DrawDebugSphere(GetWorld(), Location, 32.0, 12, FColor::Red, false, 10.0);
	FVector Direction = Location - GetActorLocation();

	Direction.Normalize();
	FRotator prevLookAt = lookAtRotation;
	lookAtRotation = FRotationMatrix::MakeFromX(Direction).Rotator();
	lookAtRotation.Pitch = 0.0;
	lookAtRotation.Roll = 0.0;
	if (lookAtRotation.Yaw == prevLookAt.Yaw ) return;
	SetActorRotation(lookAtRotation);
	RotationStep = (originalRotation.Yaw - lookAtRotation.Yaw) / (RotatingVelocity);
	GetWorldTimerManager().ClearTimer(timerHandler);
	GetWorldTimerManager().SetTimer(timerHandler,this, &AFPSDwarfGuard::RotateBack,.1,true);
	SetGuartMode(EGuardMode::Suspicios);
}

void AFPSDwarfGuard::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	SensingComp->OnSeePawn.AddDynamic(this, &AFPSDwarfGuard::OnDwarfSeePawn);
}

void AFPSDwarfGuard::RotateBack()
{
	if (guardMode == EGuardMode::Alert) return;
	FRotator CurrentRotation = GetActorRotation();
	if (RotatingVelocity > 0)
	{
		FRotator newRotation = CurrentRotation;
		newRotation.Yaw += RotationStep;
		if (std::abs(newRotation.Yaw - originalRotation.Yaw) < std::abs(RotationStep))
		{
			newRotation = originalRotation;
			lookAtRotation = originalRotation;
			GetWorldTimerManager().ClearTimer(timerHandler);
			SetGuartMode(EGuardMode::Idle);
		}
		SetActorRotation(newRotation);
	}
}

void AFPSDwarfGuard::OnRep_GuardMode()
{
	OnGuardModeChange(guardMode);
}

void AFPSDwarfGuard::SetGuartMode(EGuardMode newMode)
{
	if (guardMode == newMode) return;
	guardMode = newMode;
	if (guardMode == EGuardMode::Idle)
	{
		MoveToNextPoint();
	}
	else
	{
		GetController()->StopMovement();
	}
	OnRep_GuardMode();
}

void AFPSDwarfGuard::MoveToNextPoint()
{
	
	if (pointIndex < tpoints.Num())
	{
		//UNavigationSystem::SimpleMoveToActor(GetController(), tpoints[pointIndex]);
		UAIBlueprintHelperLibrary::SimpleMoveToActor(GetController(), tpoints[pointIndex]);
	}

}

// Called every frame
void AFPSDwarfGuard::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (pointIndex < tpoints.Num())
	{
		float Dist = FVector::DistSquared(tpoints[pointIndex]->GetActorLocation(), GetActorLocation());
		if (Dist < 6000)
		{
			pointIndex = ((pointIndex + 1) % tpoints.Num());
			MoveToNextPoint();
		}
	}
}

void AFPSDwarfGuard::GetLifetimeReplicatedProps(TArray< FLifetimeProperty >& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AFPSDwarfGuard, guardMode);
}