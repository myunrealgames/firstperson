// Fill out your copyright notice in the Description page of Project Settings.

#include "FPSLancherActor.h"
#include "Components/BoxComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/ArrowComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Classes/Particles/ParticleSystem.h"
#include "FirstPersonCharacter.h"
// Sets default values
AFPSLancherActor::AFPSLancherActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	overlapBox = CreateDefaultSubobject<UBoxComponent>(TEXT("overlapBox"));
	overlapBox->SetCollisionResponseToAllChannels(ECR_Overlap);
	RootComponent = overlapBox;
	overlapBox->OnComponentBeginOverlap.AddDynamic(this, &AFPSLancherActor::Lanch);

	mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("mesh"));
	mesh->SetCollisionResponseToChannels(ECollisionResponse::ECR_Ignore);
	mesh->SetupAttachment(overlapBox);

	plane = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("plane"));
	plane->SetCollisionResponseToChannels(ECollisionResponse::ECR_Ignore);
	plane->SetupAttachment(overlapBox);

	particleEffect = CreateDefaultSubobject<UParticleSystem>(TEXT("particle"));

}

// Called when the game starts or when spawned
void AFPSLancherActor::BeginPlay()
{
	Super::BeginPlay();
	
}

void AFPSLancherActor::Lanch(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	AFirstPersonCharacter* chracter = Cast<AFirstPersonCharacter>(OtherActor);
	FRotator launchDir = GetActorRotation();
	launchDir.Pitch += launchPitch;
	FVector direction = launchDir.Vector() * launchSpeed;
	if (chracter)
	{
		chracter->LaunchCharacter(direction, true, true);
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), particleEffect, GetActorLocation());
	}
	else if (OtherComp && OtherComp->IsSimulatingPhysics())
	{
		OtherComp->AddImpulse(direction,NAME_None,true);
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), particleEffect, GetActorLocation());
	}
}

// Called every frame
void AFPSLancherActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

