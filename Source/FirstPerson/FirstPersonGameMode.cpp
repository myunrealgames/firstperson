// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "FirstPersonGameMode.h"
#include "FirstPersonHUD.h"
#include "FirstPersonCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include <Kismet/GameplayStatics.h>
#include "FPSStateBase.h"
#include "Classes/Engine/World.h"

AFirstPersonGameMode::AFirstPersonGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AFirstPersonHUD::StaticClass();
	GameStateClass = AFPSStateBase::StaticClass();
}

void AFirstPersonGameMode::MissionCompleted(APawn* InstigatorPawn, bool bMissionSucceed)
{
	if (InstigatorPawn)
	{
		TArray<AActor*> cameraActors;
		UGameplayStatics::GetAllActorsOfClass(this, MissionViewPointActor, cameraActors);
		if (cameraActors.Num() > 0)
		{
			for (FConstPlayerControllerIterator it = GetWorld()->GetPlayerControllerIterator(); it; it++)
			{
				APlayerController* playerController = it->Get();
				if (playerController)
				{
					playerController->SetViewTargetWithBlend(cameraActors[0], 0.5, EViewTargetBlendFunction::VTBlend_Cubic);
				}
			}
		}

	}
	AFPSStateBase* GS = GetGameState<AFPSStateBase>();
	GS->MultiCastOnMissionComplete(InstigatorPawn, bMissionSucceed);
	OnMissionCompleted(InstigatorPawn, bMissionSucceed);
	GetWorld()->ServerTravel( "/Game/FirstPersonCPP/Maps/FirstPersonExampleMap?listen");
}
