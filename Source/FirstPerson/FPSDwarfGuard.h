// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "FPSDwarfGuard.generated.h"
UENUM(BlueprintType)
enum class EGuardMode : uint8
{
	Idle,
	Suspicios,
	Alert
};

class UPawnSensingComponent;
class ATargetPoint;
UCLASS()
class FIRSTPERSON_API AFPSDwarfGuard : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AFPSDwarfGuard();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UPROPERTY(VisibleAnywhere, Category = "AI")
	UPawnSensingComponent* SensingComp;
	UFUNCTION()
	void OnDwarfSeePawn(APawn* Pawn);
	UFUNCTION()
	void DwarfHeardNoise(APawn* NoiseInstigator, const FVector& Location, float Volume);
	virtual void  PostInitializeComponents() override;
	FRotator originalRotation;
	FRotator lookAtRotation;
	UPROPERTY(EditInstanceOnly, Category = "AI")
	int RotatingVelocity;
	float RotationStep;
	void RotateBack();
	FTimerHandle timerHandler;
	UPROPERTY(ReplicatedUsing=OnRep_GuardMode)
	EGuardMode guardMode;
	UFUNCTION()
	void OnRep_GuardMode();
	void SetGuartMode(EGuardMode newMode);
	UPROPERTY(EditInstanceOnly, Category = "AI")
	TArray<ATargetPoint*> tpoints;
	int pointIndex;
	void MoveToNextPoint();
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UFUNCTION(BlueprintImplementableEvent, Category = "AI")
	void OnGuardModeChange(EGuardMode newMode);

};
