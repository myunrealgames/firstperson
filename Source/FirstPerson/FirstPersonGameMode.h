// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "FirstPersonGameMode.generated.h"

UCLASS(minimalapi)
class AFirstPersonGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AFirstPersonGameMode();
	void MissionCompleted(APawn* InstigatorPawn, bool bMissionSucceed);
	UFUNCTION(BlueprintImplementableEvent, Category = "GameMode")
	void OnMissionCompleted(APawn* InstigatorPawn, bool bMissionSucceed);
protected:
	UPROPERTY(EditDefaultsOnly, Category = "ViewPoint")
	TSubclassOf<AActor> MissionViewPointActor; 
};



