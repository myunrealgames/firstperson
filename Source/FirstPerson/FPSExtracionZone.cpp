// Fill out your copyright notice in the Description page of Project Settings.

#include "FPSExtracionZone.h"
#include "Components/BoxComponent.h"
#include "Components/DecalComponent.h"
#include "FirstPersonCharacter.h"
#include "FirstPersonGameMode.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AFPSExtracionZone::AFPSExtracionZone()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	Zone = CreateDefaultSubobject<UBoxComponent>(TEXT("Zone"));
	RootComponent = Zone;
	Zone->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	Zone->SetCollisionResponseToAllChannels(ECR_Ignore);
	Zone->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	Zone->SetBoxExtent(FVector(200.0));
	Zone->OnComponentBeginOverlap.AddDynamic(this, &AFPSExtracionZone::ZoneEntered);
	Decal = CreateDefaultSubobject<UDecalComponent>(TEXT("Decal"));
	Decal->DecalSize = FVector(200.0);
	Decal->SetupAttachment(Zone);
}

void AFPSExtracionZone::ZoneEntered(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	AFirstPersonCharacter* character = Cast<AFirstPersonCharacter>(OtherActor);
	if (!character)
	{
		return;
	}
	if (character->isCarryingObjective)
	{
		AFirstPersonGameMode* FpsGameMode = Cast<AFirstPersonGameMode>(GetWorld()->GetAuthGameMode());
		if (FpsGameMode)
		{
			FpsGameMode->MissionCompleted(character, true);
		}
	}
	else
	{
		UGameplayStatics::PlaySound2D(this, missingObjectiveSound);
	}
}


