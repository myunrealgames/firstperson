// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FPSExtracionZone.generated.h"
class UBoxComponent;
UCLASS()
class FIRSTPERSON_API AFPSExtracionZone : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFPSExtracionZone();

protected:
	// Called when the game starts or when spawned
	UPROPERTY(VisibleAnywhere, Category = "Components")
	UBoxComponent* Zone;
	UPROPERTY(VisibleAnywhere, Category = "Components")
	UDecalComponent* Decal;
	UFUNCTION()
	void ZoneEntered(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UPROPERTY(EditDefaultsOnly, Category = "Components")
	USoundBase* missingObjectiveSound;
};
