// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BlackHoleActor.generated.h"
class USphereComponent;
UCLASS()
class FIRSTPERSON_API ABlackHoleActor : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ABlackHoleActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UPROPERTY(VisibleAnywhere, Category = "Components")
		UStaticMeshComponent* MeshComp;
	UPROPERTY(VisibleAnywhere, Category = "Components")
		USphereComponent* OuterSpher;
	UPROPERTY(VisibleAnywhere, Category = "Components")
		USphereComponent* InnerSpher;
	UPROPERTY(EditDefaultsOnly, Category = "Effects")
		UParticleSystem* PickupFX;
	void PlayEffect();

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UFUNCTION()
	void BlackHoleSucktion(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
};